import time

import cv2
import numpy as np

blue = [98, 232, 120]
blue_lower = np.array([blue[0] - 15, blue[1] - 30, 0])
blue_higher = np.array([blue[0] + 15, blue[1] + 30, 255])

cam = cv2.VideoCapture(0)
cam.set(cv2.CAP_PROP_AUTO_EXPOSURE, 1)
cam.set(cv2.CAP_PROP_EXPOSURE, 500)

pix_mm = 0
prev_coord = (0, 0)
dst = 0


def current_milli_time():
    return round(time.time() * 1000)


time_prev = current_milli_time()
speed = 0

while cam.isOpened():
    ret, frame = cam.read()
    frame = cv2.flip(frame, 1)
    blurred = cv2.GaussianBlur(frame, (11, 11), 0)
    hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)

    mask_blue = cv2.inRange(hsv, blue_lower, blue_higher)
    mask_blue = cv2.erode(mask_blue, None, iterations=2)
    mask_blue = cv2.dilate(mask_blue, None, iterations=2)
    contours_blue, _ = cv2.findContours(mask_blue, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    if len(contours_blue) > 0:
        c = max(contours_blue, key=cv2.contourArea)
        (x, y), radius = cv2.minEnclosingCircle(c)

        if radius > 50:
            cv2.circle(frame, (int(x), int(y)), int(radius), (255, 0, 0), 2)
            dx = x - prev_coord[0]
            dy = y - prev_coord[1]
            print(dx, dy)
            pix_mm = radius*2 / 73
            dst = (dx ** 2 + dy ** 2) ** 0.5 / pix_mm
            speed = dst / (current_milli_time() - time_prev) * 1000
            time_prev = current_milli_time()
            prev_coord = (x, y)

    # cv2.imshow("Mask_blue", mask_blue)

    cv2.putText(frame, f"Speed: {round(speed/100, 1)} m/s", (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0))
    cv2.imshow("Camera", frame)
    key = cv2.waitKey(1)
