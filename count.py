import random

import cv2
import matplotlib.pyplot as plt
import numpy as np
from skimage.morphology import binary_closing, binary_dilation, binary_erosion, binary_opening
from numba import njit
from scipy.ndimage import distance_transform_edt
from skimage.measure import label, regionprops_table


@njit
def find_peaks(cmap):
    peaks = []
    for i in range(1, cmap.shape[0] - 1):
        for j in range(1, cmap.shape[1] - 1):
            sub = cmap[i - 1:i + 2, j - 1:j + 2]
            if np.count_nonzero(sub < sub[1, 1]) == 8:
                peaks.append((i, j))
    return peaks


type1 = np.ones((6, 6))

cam = cv2.VideoCapture('balls.mp4')

frame_width = int(cam.get(3))
frame_height = int(cam.get(4))
out = cv2.VideoWriter('outpy.avi',cv2.VideoWriter_fourcc('M','J','P','G'), 25, (frame_width,frame_height))
while cam.isOpened():
    balls = 0

    ret, frame = cam.read()

    if not ret:
        break
    frame = cv2.flip(frame, 1)
    gray1 = frame[:, :, 0]

    ret1, bin1 = cv2.threshold(gray1, 100, 255, cv2.THRESH_BINARY)

    mask1 = cv2.erode(bin1, type1, iterations=10)
    mask1 = cv2.dilate(mask1, type1, iterations=2)
    # cv2.imshow("mask1", mask1)
    labeled1 = label(mask1)
    for i in range(1, labeled1.max() + 1):
        mask_new = labeled1.copy()
        mask_new[mask_new != i] = 0
        mask_new = mask_new.astype('uint8')
        contours, _ = cv2.findContours(mask_new, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        if len(contours) > 0:
            c = max(contours, key=cv2.contourArea)
            (x, y), radius = cv2.minEnclosingCircle(c)

            if radius > 20:
                balls += 1
                cv2.circle(frame, (int(x), int(y)), int(radius), (255, 0, 0), 2)

    gray1 = frame[:, :, 2]
    # cv2.imshow("CH1", ch1)
    # cv2.imshow("CH2", ch2)

    ret1, bin1 = cv2.threshold(gray1, 50, 255, cv2.THRESH_BINARY)

    mask1 = cv2.erode(bin1, type1, iterations=10)
    mask1 = cv2.dilate(mask1, type1, iterations=2)
    #cv2.imshow("mask2", mask1)
    labeled1 = label(mask1)
    for i in range(1, labeled1.max() + 1):
        mask_new = labeled1.copy()
        mask_new[mask_new != i] = 0
        mask_new = mask_new.astype('uint8')
        contours, _ = cv2.findContours(mask_new, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        if len(contours) > 0:
            c = max(contours, key=cv2.contourArea)
            (x, y), radius = cv2.minEnclosingCircle(c)

            if radius > 20:
                balls += 1
                cv2.circle(frame, (int(x), int(y)), int(radius), (0, 0, 255), 2)

    cv2.putText(frame, f"Balls: {balls}", (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0))

    cv2.imshow("Camera", frame)
    out.write(frame)
    key = cv2.waitKey(1)

cam.release()
out.release()